## [1.0.1](https://gitlab.com/claudio.junior/semantic-release/compare/v1.0.0...v1.0.1) (2021-11-18)


### Bug Fixes

* **eso-035:** exemplo de fix ([65bad8a](https://gitlab.com/claudio.junior/semantic-release/commit/65bad8ac927ea65e0152fc13628cb786b8052984))

# 1.0.0 (2021-11-18)


### Features

* semantic-release ([98fb3ec](https://gitlab.com/claudio.junior/semantic-release/commit/98fb3ecb9be06dc1ed7036aa70dff89b199f9984))
* **eso-035:** criando um arquivo teste ([de60536](https://gitlab.com/claudio.junior/semantic-release/commit/de605364b7ce1ac976a36b9ef4bc1ad79a1cc9a4))
